import React, { Component } from 'react'
import {
  Button,
  Col,
  Collapse,
  Form,
  FormGroup,
  Input,
  Label,
  Nav,
  Navbar,
  NavbarBrand,
  Row,
  Table,
  Card,
  CardText,
  NavItem,
  NavLink
} from 'reactstrap'
import JSONConverter from "silvers_json_converter"
import './App.css'

let alphabet = "abcdefghijklmnopqrstuvwxyz"
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textBox: "",
      result:0,
      results:[],
      startNumber:1,
      endNumber:26,
      collapse: false
    }
    this.toggle = this.toggle.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.handleTextBox = this.handleTextBox.bind(this)

    this.handleNumberBoxStart = this.handleNumberBoxStart.bind(this)
    this.handleNumberBoxEnd = this.handleNumberBoxEnd.bind(this)


  }
  toggle() {
    this.setState({
      collapse: !this.state.collapse
    });
  }

  handleTextBox(event) {
    this.setState({textBox: event.target.value});
  }

  handleNumberBoxStart(event) {
    if(event.target.value <= this.state.endNumber || event.target.value === ""){
      this.setState({startNumber: event.target.value})
    }
  }

  handleNumberBoxEnd(event) {
    if(event.target.value >= this.state.startNumber || event.target.value === ""){
      this.setState({endNumber: event.target.value})
    }
  }

  onSubmit() {
    let result = this.calculate(this.state.textBox)
    let results = this.state.results
    results.push({text:this.state.textBox, result:result, start:this.state.startNumber, end:this.state.endNumber})
    this.setState({ textBox:"", result: result, results:results});
  }

  calculate(text){
    let result = 0
    let words = text.split(" ")
    for(let i=0;i<words.length;i++){
      for(let j=0;j<words[i].length;j++){
        result += this.convertLetterToNumber(words[i][j])
      }
    }
    return result
  }

  convertLetterToNumber(letter){
    let cleaned = letter.toLowerCase()
    let start = this.state.startNumber -0
    let end = this.state.endNumber -0

    let result = start
    let letters = alphabet.split("")

    if(letters.indexOf(cleaned) === -1){ return 0}

    for(let i=0;i<letters.length;i++){
      if(cleaned === letters[i]){return result}
      result++
      if(result > end){result = start}
    }
  }

  createTable(){
    let data = this.state.results
    let body = []

    if(data.length ===0){return [null,null]}

    for(let i=0;i<data.length;i++){
      body.push(
        <tr key={i} >
          <td>{data[i].text}</td>
          <td>{data[i].result}</td>
          <td>{data[i].start}</td>
          <td>{data[i].end}</td>
        </tr>)
    }

    let headers = [
      {title:"Sentence",key:"text",type:"string"},
      {title:"Result",key:"result",type:"number"},
      {title:"Start",key:"start",type:"number"},
      {title:"End",key:"end",type:"number"}
    ]
    let download = <JSONConverter
      data={data}
      type={"CSV"}
      headers={headers}
      separator={","}
      text={"Download Results"}
      name={"Conversion_"+ new Date().toISOString()}
    />
    let table =  <Table responsive dark striped>
      <thead>
      <tr>
        <th>Sentence</th>
        <th>Result</th>
        <th>Start</th>
        <th>End</th>
      </tr>
      </thead>
      <tbody>
      {body}
      </tbody>
    </Table>
    return [download, table]
  }

  render() {
    let navBar = <Navbar color="dark" dark expand="md">
      <NavbarBrand href="/">Home</NavbarBrand>
        <Nav navbar>
          <NavItem>
            <NavLink onClick={this.toggle}>Info</NavLink>
          </NavItem>
        </Nav>
    </Navbar>

    let button = <Button onClick={() => this.onSubmit(1)}>Convert</Button>
    if(this.state.startNumber === "" || this.state.endNumber === ""){
      button = <Button variant="danger" disabled>Invalid Number</Button>
    }

    let input =  <Form>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="startNumberBox">
              Start. <span style={{"fontSize":"0.5em"}}>A={this.convertLetterToNumber("a")}</span>
            </Label>
            <Input type="number" name="startNumber" id="startNumberBox"  value={this.state.startNumber} onChange={this.handleNumberBoxStart} />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="endNumberBox">
              End. <span style={{"fontSize":"0.5em"}}>Z={this.convertLetterToNumber("z")}</span>
            </Label>
            <Input type="number" name="endNumber" id="endNumberBox" value={this.state.endNumber} onChange={this.handleNumberBoxEnd} />
          </FormGroup>
        </Col>
      </Row>
      <Row form>
        <Col md={10}>
          <FormGroup>
            <Label for="textBox">Enter Sentence</Label>
            <Input type="textarea" name="text" id="textBox" placeholder={"Enter text."} value={this.state.textBox} onChange={this.handleTextBox} />
          </FormGroup>
        </Col>
        <Col md={2}>
          <br />
          {button}
        </Col>
      </Row>
    </Form>

    let info =  <div>
      <Collapse isOpen={this.state.collapse}>
        <Card body inverse style={{ backgroundColor: '#3e444b', borderColor: '#3e444b'}}>
          <CardText>
            <span style={{ fontSize:"medium" }}>
              This is a converter to turn letters into numbers and then sum up to one number.
              <br />
              If you have any questions drop me <a href="mailto:brendan.golden.23+converter.brendán.ie@gmail.com" rel={"noopener noreferrer"}>an email.</a>
              <br />
              If this has helped feel free to top up my <a href={"https://paypal.me/BrendanGolden"} target={"_blank"} rel={"noopener noreferrer"} >coffee fund.</a>
            </span>
          </CardText>
        </Card>
      </Collapse>
    </div>
    let [download,table] = this.createTable()
    return (
      <div className="App">
        {navBar}
        <header className="App-header">
          {info}
          <br />
          {input}
          <br />
          {download}
          <br />
          {table}
        </header>
      </div>
    );
  }
}

export default App;
